# generator-benjamin-van-ryseghem [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Generate a JS project skeleton

## Installation

First, install [Yeoman](http://yeoman.io) and generator-benjamin-van-ryseghem using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-benjamin-van-ryseghem
```

Then generate your new project:

```bash
yo benjamin-van-ryseghem
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Benjamin Van Ryseghem](https://benjamin.vanryseghem.com)


[npm-image]: https://badge.fury.io/js/generator-benjamin-van-ryseghem.svg
[npm-url]: https://npmjs.org/package/generator-benjamin-van-ryseghem
[travis-image]: https://travis-ci.org/BenjaminVanRyseghem/generator-benjamin-van-ryseghem.svg?branch=master
[travis-url]: https://travis-ci.org/BenjaminVanRyseghem/generator-benjamin-van-ryseghem
[daviddm-image]: https://david-dm.org/BenjaminVanRyseghem/generator-benjamin-van-ryseghem.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/BenjaminVanRyseghem/generator-benjamin-van-ryseghem
