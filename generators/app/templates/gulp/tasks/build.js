const gulp = require("gulp");
const connect = require("gulp-connect");
const rename = require("gulp-rename");
const del = require("del");
const vinylPaths = require("vinyl-paths");
const minimist = require("minimist");
const config = require("../config").watch;

const knownOptions = {
	string: "name"
};

const options = minimist(process.argv.slice(2), knownOptions);

gulp.task("build", ["browserify", "styles"], () => {
	gulp.src(config.src).pipe(connect.reload());
});

gulp.task("build:edge", ["browserify:min"], () => gulp.src("./dist/seatlib**.js**")
	.pipe(vinylPaths(del))
	.pipe(rename((path) => {
		path.basename = path.basename.replace("seatlib", "latest");
	}))
	.pipe(gulp.dest("./dist")));

gulp.task("build:rc", ["browserify:min"], () => {
	del(["./dist/seatlib.min.js.map"]);

	return gulp.src("./dist/seatlib.min.js")
		.pipe(vinylPaths(del))
		.pipe(rename("rc.min.js"))
		.pipe(gulp.dest("./dist"));
});

gulp.task("build:stable", ["browserify:min"], () => {
	del(["./dist/seatlib.min.js.map"]);

	return gulp.src("./dist/seatlib.min.js")
		.pipe(vinylPaths(del)) // todo: sync with Flo
		.pipe(rename("stable.min.js"))
		.pipe(gulp.dest("./dist"));
});

gulp.task("build:preprod", ["browserify:min"], () => {
	del(["./dist/seatlib.min.js.map"]);

	return gulp.src("./dist/seatlib.min.js")
		.pipe(vinylPaths(del)) // todo: sync with Flo
		.pipe(rename(`${options.name}.min.js`))
		.pipe(gulp.dest("./dist"));
});
