const Generator = require("yeoman-generator");
const chalk = require("chalk");
const yosay = require("yosay");
const askName = require("inquirer-npm-name");
const extend = require("deep-extend");
const path = require("path");
const fs = require("fs");

const devDependencies = {
	babelify: "^7.3.0",
	browserify: "^10.1.2",
	eslint: "^4.1.0",
	"eslint-config-benjamin-van-ryseghem": "^1.0.1",
	gulp: "^3.9.1",
	"gulp-connect": "^5.0.0",
	"gulp-jasmine": "^2.4.2",
	"gulp-rename": "^1.2.2",
	"gulp-sourcemaps": "^2.6.1",
	"gulp-uglify": "^3.0.0",
	"gulp-util": "^3.0.4",
	"jasmine-core": "^2.8.0",
	minimist: "^1.2.0",
	"require-dir": "^0.3.2",
	"vinyl-buffer": "^1.0.0",
	"vinyl-paths": "^2.1.0",
	"vinyl-source-stream": "^1.1.0"
};

module.exports = class extends Generator {
	initializing() {
		this.props = {};
	}

	prompting() {
		// Have Yeoman greet the user.
		this.log(yosay(`Welcome to the well-made ${chalk.red("generator-benjamin-van-ryseghem")} generator!`));

		if (this.options.offline) {
			const prompts = [
				{
					type: "string",
					name: "name",
					message: "Your app name",
					default: "My awesome app"
				}
			];

			return this.prompt(prompts).then((props) => Object.assign(this.props, props));
		}

		return askName({
			name: "name",
			message: "Your app name",
			default: "My awesome app",
			validate: (str) => !!str.length
		}, this).then(
			(props) => { this.props.name = props.name; },
			() => {
				this.log("npm can't be reached. Thus the app name availability can't be checked.");
				process.exit(1); // eslint-disable-line no-process-exit
			}
		);
	}

	default() {
		const readmeTpl = this.fs.read(path.join(this.sourceRoot(), "../README.md"));
		this.composeWith(require.resolve("generator-node/generators/app"), {
			coveralls: false,
			boilerplate: false,
			editorconfig: false,
			projectRoot: "src",
			travis: false,
			eslint: false,
			name: this.props.name,
			skipInstall: this.options.skipInstall,
			readme: readmeTpl
		});
	}

	webApp() {
		const prompts = [
			{
				type: "confirm",
				name: "web",
				message: "Is it a web app",
				default: true
			},
			{
				type: "input",
				name: "port",
				message: "Which port to serve the app",
				default: 8080,
				validate: (value) => {
					if (isNaN(+value)) {
						throw new Error("`port should be a number`");
					}
					return true;
				},
				filter: (value) => +value,
				when: (props) => props.web
			}
		];

		return this.prompt(prompts).then((props) => Object.assign(this.props, props));
	}

	writing() {
		this._writingFiles(this.templatePath());
	}

	_writingFiles(folder, root = "") {
		const children = fs.readdirSync(folder); // eslint-disable-line no-sync
		children.forEach((child) => {
			const childPath = path.join(folder, child);
			const stat = fs.statSync(childPath); // eslint-disable-line no-sync
			if (stat.isDirectory()) {
				this._writingFiles(childPath, path.join(root, child));
			}

			if (stat.isFile()) {
				const name = this._getNameFor(child);
				if (name === null) {
					return;
				}

				this.fs.copyTpl(
					this.templatePath(childPath),
					this.destinationPath(path.join(root, name)),
					this.props
				);
			}
		});
	}

	_getNameFor(file) {
		let name = file.replace(/\.ejs$/, "");

		const match = name.match(/^_([^\]]+)_(.+)$/);
		if (match) {
			const [_, property, fileName] = match;
			if (property && fileName && this.props[property]) {
				name = fileName;
			} else {
				return null;
			}
		}

		if (name[0] === "_") {
			const [_, ...rest] = name;
			name = rest.join("");
		}

		return name;
	}

	_beforeInstall() {
		const pkg = this.fs.readJSON(this.destinationPath("package.json"), {});

		delete pkg.devDependencies;
		delete pkg.dependencies;
		delete pkg.scripts;
		delete pkg["lint-staged"];
		delete pkg.eslintConfig;
		delete pkg.jest;

		extend(pkg, {
			devDependencies,
			scripts: {
				test: "gulp test"
			}
		});

		fs.writeFileSync(this.destinationPath("package.json"), JSON.stringify(pkg)); // eslint-disable-line no-sync
	}

	install() {
		this._beforeInstall();
		this.installDependencies({
			bower: false,
			yarn: true,
			npm: false
		});
	}
};
